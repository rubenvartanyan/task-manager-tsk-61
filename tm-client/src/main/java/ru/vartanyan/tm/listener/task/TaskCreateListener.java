package ru.vartanyan.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.event.ConsoleEvent;
import ru.vartanyan.tm.listener.AbstractTaskListener;
import ru.vartanyan.tm.endpoint.SessionDTO;
import ru.vartanyan.tm.exception.empty.EmptyDescriptionException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.util.TerminalUtil;

@Component
public class TaskCreateListener extends AbstractTaskListener {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "task-create";
    }

    @Override
    public @Nullable String description() {
        return "Create task";
    }

    @Override
    @EventListener(condition = "@taskCreateListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        @Nullable final SessionDTO session = bootstrap.getSession();
        if (session == null) throw new NotLoggedInException();
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        @NotNull String name = TerminalUtil.nextLine();
        if ((name).isEmpty()) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull String description = TerminalUtil.nextLine();
        if ((description).isEmpty()) throw new EmptyDescriptionException();
        taskEndpoint.addTask(name, description, session);
        System.out.println("[TASK CREATED]");
    }
}
